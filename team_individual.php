<?php
/*
Risus Web. Copyright (c) 2016 Robin Phillips
This software may be modified and distributed under the terms
of the MIT license.  See the LICENSE file for details.

Risus: The Anything RPG is written by S. John Ross. Get it from
https://www.drivethrurpg.com/product/170294/
*/

require("inc_head_php.php");
require("inc_head_html.php");
require("inc_team.php");

// Get team members
if (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] != "") {
	$teamcliches = array();
	$teamdice = array();
	// Initialise inappropriate and vengeance to 0. Set to 1 later if required
	$teaminappropriate = 0;
	$team0vengeance = 0;
	$team1vengeance = 0;

	// Get "inappropriate" state and ensure that "member" checkbox is set to checked for leader
	if (isset($_POST["leader0"])) {
		$_POST["member".intval($_POST["leader0"])] = intval($_POST["leader0"]);
		if (isset($_POST["inappropriate0"]))
			$teaminappropriate = intval($_POST["inappropriate0"]);
		if (isset($_POST["vengeance0"]))
			$team0vengeance = intval($_POST["vengeance0"]);
	}
	else {
		$_POST["member".intval($_POST["leader1"])] = intval($_POST["leader1"]);
		if (isset($_POST["inappropriate1"]))
			$teaminappropriate = intval($_POST["inappropriate1"]);
		if (isset($_POST["vengeance1"]))
			$team1vengeance = intval($_POST["vengeance1"]);
	}
	
	// Find team members and add relevant cliché & dice to $teamcliches & $teamdice array
	foreach ($_POST as $key=>$value) {
		if (substr($key, 0, 6) == "member" && strlen($key) > 6) {
			// charid: clicheid
			$teamcliches[substr($key, 6)] = $_POST["cliche".intval($value)];
			
			$clichecurrent = $db->querySingle("SELECT current FROM cliches WHERE clicheid=".$_POST["cliche".intval($value)]);
			// charid: NumberOfDice
			$teamdice[intval($value)] = $clichecurrent;
			if (isset($_POST["lucky".intval($value)]))
				$teamdice[intval($value)]++;
		}
	}

	$log = "<p>Team vs Individual</p>\n<p>";
	$teamtotal = 0;

	// Roll team dice
	if ($_POST["teamtype"] == "character")
		$teamtotal = roll_team_dice ($db, $teamdice, $_POST["leader0"], $team0vengeance, $log);
	else
		$teamtotal = roll_team_dice ($db, $teamdice, $_POST["leader1"], $team1vengeance, $log);
	$log .= "Team total is <strong>$teamtotal</strong>.</p>\n";

	// Individual's dice roll
	$individual = $db->querySingle("SELECT name FROM characters WHERE charid = ".$_POST["individual"]);
	$individualcliche = $db->querySingle("SELECT * FROM cliches WHERE clicheid = ".$_POST["individualcliche"], True);
	$log .= "<p>".htmlentities($individual,ENT_QUOTES)." (".htmlentities($individualcliche["cliche"],ENT_QUOTES)." ".$individualcliche["current"].")<br>";
	// Inappropriate Clichés and Lucky Shots
	if (isset($_POST["inappropriate"]))
		$log .= htmlentities($individual,ENT_QUOTES)." is using an Inappropriate Cliché<br>";
	if (isset($_POST["lucky"])) {
		$individualdice = $individualcliche["current"] +1;
		$log .= htmlentities($individual,ENT_QUOTES)." is using a Lucky Shot<br>";
	}
	else
		$individualdice = $individualcliche["current"];

	$individualroll = dice_roll($individualdice);
	$log .= htmlentities($individual,ENT_QUOTES)." rolls <strong>" . array_sum($individualroll) . "</strong> (";
	foreach ($individualroll as $die)
		$log .= "$die, ";
	// Remove final comma-space
	$log = substr($log, 0, -2) . ")</p>\n<p>";

	// Work out results
	if ($teamtotal == array_sum($individualroll))
		$log .= "Draw!";
	elseif ($teamtotal > array_sum($individualroll)) {
		// Team won
		$log .= "Team wins.<br>";
		$log .= htmlentities($individual,ENT_QUOTES)." loses ";
		if ($teaminappropriate == 1 && !isset($_POST["inappropriate"])) {
			$log .= "<i>three points</i>";
			$newindividualcliche = $individualcliche["current"] -3;
		}
		else {
			$newindividualcliche = $individualcliche["current"] -1;
			$log .= "one point";
		}
		$log .= " from ".htmlentities($individualcliche["cliche"],ENT_QUOTES);
		if ($newindividualcliche <= 0) {
			$newindividualcliche = 0;
			$log .= "<br><b>".htmlentities($individual,ENT_QUOTES)." is defeated!</b>";
		}
		// Update individual's cliché
		$updateclichesql = "UPDATE cliches SET current = $newindividualcliche WHERE clicheid = ".$_POST["individualcliche"];
		$db->exec($updateclichesql);
	}
	else {
		// Individual won
		$log .= htmlentities($individual,ENT_QUOTES)." wins.";

		// $viewlog is HTML to be displayed but not logged
		$viewlog = "Choose a team member to lose ";
		if ($teaminappropriate == 0 && isset($_POST["inappropriate"])) {
			$viewlog .= "<i>three points:</i>";
			$teamclicheloss = 3;
		}
		else {
			$viewlog .= "one point:";
			$teamclicheloss = 1;
		}

		$viewlog .= "<br><select id='updateteamcliche' data-clicheloss='$teamclicheloss'>";
		foreach ($teamcliches as $charid=>$clicheid) {
			$name = $db->querySingle("SELECT name FROM characters WHERE charid = $charid");
			$cliche = $db->querySingle("SELECT cliche, current FROM cliches WHERE clicheid = $clicheid", True);
			$viewlog .= "<option value='$clicheid' id='option$clicheid' data-charid='$charid' data-current='".intval($cliche["current"])."'>";
			$viewlog .= htmlentities($name, ENT_QUOTES)." (".htmlentities($cliche["cliche"], ENT_QUOTES)." ".intval($cliche["current"]).")";
			$viewlog .= "</option>";
		}
		$viewlog .= "</select>";
		$viewlog .= "&nbsp;<button id='selectbutton'>Select</button><br>";
		$viewlog .= "<input type = 'checkbox' name='stepforward' id='stepforward' value = '1'> <label for ='stepforward' title='The character takes double damage, but the team leader gets double dice on the next roll' id='lblStepForward'>Character is stepping forward</label>";
	}
	$log .= "</p>\n";
	$viewlog .= "</p>\n";
	logdb ($log);
}
?>
<script>
$(function() {
	// Update cliché lists when individual changes
	$("#individual").change(function(event){
		$('#individualcliche').load('./ajax_clicheoptionlist.php?charid='+$("#individual").val())
	})

	// Hide results box on rolling dice
	$("#btnSubmit").click(function(event) {
		$("#resultsdiv").hide()
	})
})
</script>

<h1>Combat: Team vs Individual</h1>

<form method="post">
<?php
if (isset($_POST["teamtype"]) && $_POST["teamtype"] == "npc") {
	echo "<input type='hidden' id='teamtype' name='teamtype' value='npc'>\n";
	echo "<span id='currentteam'>NPC</span> Team (<a id='changeteam' href='#'>Change to Character team</a>)\n";
	$showChar = False;
	$showNPC = True;
}
else {
	echo "<input type='hidden' id='teamtype' name='teamtype' value='character'>\n";
	echo "<span id='currentteam'>Character</span> Team (<a id='changeteam' href='#'>Change to NPC team</a>)\n";
	$showChar = True;
	$showNPC = False;
}

displayCharacters($db, 0, $showChar);
displayCharacters($db, 1, $showNPC);
?>

<p>vs</p>

<div class="box">
<p class="boxtitle">Individual</p>
<p>
<select name="individual" id="individual">
<?php
if (isset($_POST["individual"]))
	$selectedindividual = selectCharacters($db, $_POST["individual"]);
else {
	$sql = "SELECT charid FROM characters WHERE active = 1 AND npc = 1 ORDER BY name LIMIT 1";
	$selectedindividual = selectCharacters($db, $db->querySingle($sql));
}
?>
</select>
<select name="individualcliche" id="individualcliche">
<?php
$sql = "SELECT * FROM cliches WHERE cliche_charid = $selectedindividual ORDER BY full DESC";
$cliches = $db->query($sql);
while ($cliche = $cliches->fetchArray(SQLITE3_ASSOC)) {
	echo "<option value='".$cliche["clicheid"]."'";
	if (isset($_POST["individualcliche"]) && $cliche["clicheid"] == intval($_POST["individualcliche"]))
		echo " selected";
	echo ">".$cliche["cliche"]." ".$cliche["current"]." ".clichevalue($cliche["full"],$cliche["doublepump"])."</option>";
}
?>
</select>
</p>
<?php
	if (isset($_POST["inappropriate"]))
		$check = " checked";
	else
		$check = "";
	?>
	<p class='inappropriate'>
	<input name="inappropriate" type="checkbox" id="inappropriate"<?=$check;?>> <label for="inappropriate">This is an Inappropriate Cliché</label>
	</p>
<p>
<input name="lucky" type="checkbox" id="lucky"> <label for="lucky">Use a Lucky Shot</label>
</p>
</div>

<p><input type="submit" name="btnSubmit" value="Roll the Dice" id="btnSubmit"></p>
</form>

<?php
if ($log != "")
	$style = "";
else
	$style = "style='display:none;'";
?>
<div class='box' id='resultsdiv' <?=$style;?>>
<h2>Results</h2>
<?php
echo "<p>$log</p>\n";
if ($viewlog != "")
	$style = "";
else
	$style = "style='display:none;'";
echo "<p id='viewlog' $style>$viewlog</p>\n";
?>
</div>

<?php
require("inc_foot.php");
?>
