<?php
/*
Risus Web. Copyright (c) 2016 Robin Phillips
This software may be modified and distributed under the terms
of the MIT license.  See the LICENSE file for details.

Risus: The Anything RPG is written by S. John Ross. Get it from
https://www.drivethrurpg.com/product/170294/
*/

require("inc_head_php.php");
require("inc_head_html.php");
$msg = "";

if (isset($_POST["btnReset"]) && $_POST["btnReset"] != "") {
	// Remove loaned clichés
	$sql = "DELETE FROM cliches WHERE loanboost LIKE 'loan'";
	$db->exec($sql);
	// Remove boost from boosted clichés
	$sql = "UPDATE cliches SET current = current-2, loanboost = NULL WHERE loanboost LIKE 'boost'";
	$db->exec($sql);
	$msg = "Boosted and loaned clichés have been reset";
}

if (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] != "") {
	// Go through characters
	$sql = "SELECT charid FROM characters WHERE npc = 0 AND active = 1";
	$pcs = $db->query($sql);
	while ($pc = $pcs->fetchArray()) {
		$sql = "SELECT COUNT(*) FROM cliches WHERE cliche_charid=".$pc["charid"]." AND cliche LIKE '".$db->escapeString($_POST["boostedcliche"])."'";
		if ($db->querySingle($sql) == 0) {
			// Add the loan cliché
			$sql = "INSERT INTO cliches (cliche_charid, cliche, full, current, loanboost) VALUES (".
				$pc["charid"].",
				'".$db->escapeString($_POST["newcliche"])."',
				2,2, 'loan')";
		}
		else {
			// Boost the existing cliché
			$sql = "UPDATE cliches SET current = current+2, loanboost='boost' WHERE cliche_charid=".$pc["charid"]." AND cliche LIKE '".$db->escapeString($_POST["boostedcliche"])."'";
		}
		$db->exec($sql);
	}
	$msg = "Clichés have been boosted and loaned";
}
?>

<h1>Loaning Clichés</h1>

<p>
Use this page to &quot;loan&quot; clichés to characters, according to the &quot;<i>When Somebody Can't Participate</i>&quot; section of Risus.
</p>

<?php
if ($msg != "")
	echo "<p class='good'>$msg</p>";
?>

<form method="post">
<?php
$sql = "SELECT COUNT(*) FROM cliches WHERE loanboost IS NOT NULL AND loanboost <>''";
if ($db->querySingle($sql) > 0) {
?>
	<div class="box">
	<p>
	There are clichés boosted &amp; on loan. Click the button below to reset them:<br>
	<input type="submit" value="Reset" name="btnReset">
	</p>
	</div><br>
<?php
}
?>

<div class="box">
<p>
Boost this cliché for those characters that have it:
<select name="boostedcliche">
<?php
$sql = "SELECT DISTINCT cliche FROM cliches LEFT JOIN characters ON cliche_charid = charid WHERE npc=0 AND active = 1";
$cliches = $db->query($sql);
while ($cliche = $cliches->fetchArray(SQLITE3_ASSOC)) {
	echo "<option value='".htmlentities($cliche["cliche"], ENT_QUOTES)."'>".htmlentities($cliche["cliche"], ENT_QUOTES)."</option>";
}
?>
</select>
</p>
<p>
Characters without the boosted cliché get the following cliché &quot;on loan&quot;:<br>
<input name="newcliche">
</p>
<p>
<input type="submit" value="Loan &amp; Boost" name="btnSubmit">
</p>
</div>
</form>

<?php
require("inc_foot.php");
?>
