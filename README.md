# Risus Web

This is a web application, intended to be used by people playing _[Risus: The Anything RPG](https://www.drivethrurpg.com/product/170294/?affiliate_id=235519)_ online. It can send updates (dice roll results etc) to an [XMPP/Jabber](https://xmpp.org/) chatroom.

A demo install is available at <https://rpg.phillipsuk.org/risuswebdemo/>

_Risus: The Anything RPG_ is written by S. John Ross. Get it from [DriveThruRPG](https://www.drivethrurpg.com/product/170294/?affiliate_id=235519)

## Requirements

PHP >= 5.5.0 with SQLite support.

The install directory must be writable by the web server, so that changes can be written to the database.

For XMPP support, [sendxmpp](http://sendxmpp.hostname.sk/) must be installed.

## Installation

* Rename/copy `inc_config_sample.php` to `inc_config.php`
* Edit `inc_config.php` to set required values
* Place all files in a web-accessible directory
* Navigate to the `install.php` file in a web browser
* Fill in the GM details
* Click the Install button

This will create the required SQLite database in the install directory, and set up a GM user. Delete the `install.php` file once installation is complete.

Once logged in as the GM user, you can add players using the "Add/Edit Player" link.

## Upgrades

To upgrade to a newer version, simply upload the files over the old ones.

## Licence

The MIT License (MIT)

Risus Web. Copyright (c) 2016 Robin Phillips

Permission is hereby granted, free of charge, to any person obtaining a copy  
of this software and associated documentation files (the "Software"), to deal  
in the Software without restriction, including without limitation the rights  
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell  
copies of the Software, and to permit persons to whom the Software is  
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all  
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE  
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  
SOFTWARE.

----

Risus: The Anything RPG is written by S. John Ross. Get it from  
https://www.drivethrurpg.com/product/170294/
