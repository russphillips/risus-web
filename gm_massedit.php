<?php
/*
Risus Web. Copyright (c) 2016 Robin Phillips
This software may be modified and distributed under the terms
of the MIT license.  See the LICENSE file for details.

Risus: The Anything RPG is written by S. John Ross. Get it from
https://www.drivethrurpg.com/product/170294/
*/

require("inc_head_php.php");
require("inc_head_html.php");
?>

<h1>Mass Update PCs/NPCs</h1>

<?php
if (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] != "") {
	// Loop through characters in database, update each one
	$sql = "SELECT charid FROM characters";
	$characters = $db->query($sql);
	while ($character = $characters->fetchArray()) {
		// Update "active" column
		if (isset($_POST["active".$character["charid"]]))
			$active = 1;
		else
			$active = 0;
		// Clear notes
		if (isset($_POST["clearnote".$character["charid"]]))
			$sql = "UPDATE characters SET active = $active, notes = '' WHERE charid=".$character["charid"];
		else
			$sql = "UPDATE characters SET active = $active WHERE charid=".$character["charid"];
		$db->exec($sql);
		
		// Update clichés
		if (isset($_POST["restore".$character["charid"]])) {
			$sql = "UPDATE cliches SET current = full WHERE cliche_charid=".$character["charid"];
			$db->exec($sql);
		}
		
		// Delete
		if (isset($_POST["delete".$character["charid"]])) {
			$sql = "DELETE FROM characters WHERE charid=".$character["charid"];
			$db->exec($sql);
		}
	}
	
	echo "<p class='good'>Characters updated.</p>";
}

function displayCharacters($db, $npc) {
	echo "<table><tr>
	<th>Name</th>
	<th>Active <input type='checkbox' id='allactive$npc' title='check/uncheck all'></th>
	<th>Restore Clichés <input type='checkbox' id='allrestore$npc' title='check/uncheck all'></th>
	<th>Clear Notes <input type='checkbox' id='allclear$npc' title='check/uncheck all'></th>
	<th>Delete Character <input type='checkbox' id='alldelete$npc' title='check/uncheck all'></th>
	</tr>";
	
	$sql = "SELECT * FROM characters WHERE npc = $npc ORDER BY name";
	$pcs = $db->query($sql);
	while ($pc = $pcs->fetchArray(SQLITE3_ASSOC)) {
		echo "<tr class='alternate'>";
		echo "<td>".htmlentities($pc["name"])."</td>";
		if ($pc["active"] == 1)
			$check = " checked";
		else
			$check = "";
		echo "<td class='check'><input class='activetick$npc' type='checkbox'$check name='active".$pc["charid"]."'></td>";
		echo "<td class='check'><input class='restoretick$npc' type='checkbox' name='restore".$pc["charid"]."'></td>";
		echo "<td class='check'><input class='clearnote$npc' type='checkbox' name='clearnote".$pc["charid"]."'></td>";
		echo "<td class='check'><input class='delete$npc' type='checkbox' name='delete".$pc["charid"]."'></td>";
		echo "</tr>";
	}
	echo "</table>";
}
?>
<script>
$(function() {
	// Change handlers for select/deselect all
	$("#allactive0").change(function () {
		$(".activetick0").prop('checked', $(this).prop("checked"));
	});
	$("#allactive1").change(function () {
		$(".activetick1").prop('checked', $(this).prop("checked"));
	});
	$("#allrestore0").change(function () {
		$(".restoretick0").prop('checked', $(this).prop("checked"));
	});
	$("#allrestore1").change(function () {
		$(".restoretick1").prop('checked', $(this).prop("checked"));
	});
	$("#allclear0").change(function () {
		$(".clearnote0").prop('checked', $(this).prop("checked"));
	});
	$("#allclear1").change(function () {
		$(".clearnote1").prop('checked', $(this).prop("checked"));
	});
	$("#alldelete0").change(function () {
		$(".delete0").prop('checked', $(this).prop("checked"));
	});
	$("#alldelete1").change(function () {
		$(".delete1").prop('checked', $(this).prop("checked"));
	});
})
</script>

<h2>Player Characters</h2>

<form method="post">
<?php
displayCharacters($db, 0);
?>

<h2>NPCs</h2>

<?php
displayCharacters($db, 1);
?>

<p><input type="submit" name="btnSubmit"></p>
</form>

<?php
require("inc_foot.php");
?>
