<?php
/*
Risus Web. Copyright (c) 2016 Robin Phillips
This software may be modified and distributed under the terms
of the MIT license.  See the LICENSE file for details.

Risus: The Anything RPG is written by S. John Ross. Get it from
https://www.drivethrurpg.com/product/170294/
*/

require("inc_head_php.php");
require("inc_head_html.php");

if (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] != "") {
	$clicheid = intval($_POST["cliche"]);
	$charid = intval($_POST["character"]);
	
	$htmlcharacter = htmlentities($db->querySingle("SELECT name FROM characters WHERE charid = $charid"),ENT_QUOTES);
	$cliche = $db->querySingle("SELECT * FROM cliches WHERE clicheid = $clicheid", True);

	$log = "<p>$htmlcharacter (".htmlentities($cliche["cliche"],ENT_QUOTES)." ".$cliche["current"].")";
	$log .= ", rolling against target number ".intval($_POST["target"])."<br>";

	$dice = $cliche["current"];
	// Lucky Shot
	if (isset($_POST["lucky"])) {
		$dice++;
		$log .= "$htmlcharacter is using a Lucky Shot<br>";
	}
	// Pump
	if (intval($_POST["pump"]) > 0) {
		$pump = intval($_POST["pump"]);
		$dice += $pump;
		$log .= "$htmlcharacter is pumping by $pump ".die_dice($pump)."<br>";
	}
	$log .= "</p>";
	
	// Roll the dice
	$roll = dice_roll($dice);
	$result = array_sum($roll) - intval($_POST["target"]);

	$log .= "<p>$htmlcharacter rolls " . array_sum($roll) . " (";
	foreach ($roll as $die)
		$log .= "$die, ";
	// Remove final comma-space
	$log = substr($log, 0, -2) . ")<br>";
	
	// Work out results
	if ($result >= 0)
		$log .= "<p class='good'>Success! Target beaten by $result";
	else
		$log .= "<p class='bad'>Failed! Target missed by " . abs($result);
	$log .= "</p>";
	
	// Reduce cliché if it was pumped
	if ($_POST["pump"] > 0) {
		$new = $cliche["current"] - $pump;
		if ($new < 0)
			$new = 0;
		$sql = "UPDATE cliches SET current = $new WHERE clicheid = $clicheid";
		$db->exec($sql);
		$log .= "<p>Pumped cliché is reduced by $pump. New value is $new</p>";
	}

	// Log the result
	logdb ($log);
}
?>
<script>
$(function() {
	// Update cliché list when character changes
	$("#character").change(function(event){
		$('#cliche').load('./ajax_clicheoptionlist.php?charid='+$("#character").val())
	});

	// Hide results box on rolling dice
	$("#btnSubmit").click(function(event) {
		$("#results").hide()
	})
})
</script>

<h1>Target Number</h1>

<form method="post">
<div class="box">
<p class="boxtitle">Character</p>
<p>
<select name="character" id="character">
<?php
if (!isset($_POST["character"])) // Character is not set. Default to player's character
	$selectedcharacter = selectCharacters($db, CHARACTERID);
else
	$selectedcharacter = selectCharacters($db, $charid);
?>
</select>
<select name="cliche" id="cliche">
<?php
$sql = "SELECT * FROM cliches WHERE cliche_charid = $selectedcharacter ORDER BY full DESC";
$cliches = $db->query($sql);
while ($cliche = $cliches->fetchArray(SQLITE3_ASSOC)) {
	echo "<option value='".$cliche["clicheid"]."'";
	if (isset($_POST["cliche"]) && $cliche["clicheid"] == $clicheid)
		echo " selected";
	echo ">".$cliche["cliche"]." ".$cliche["current"]." ".clichevalue($cliche["full"],$cliche["doublepump"])."</option>";
}
?>
</select>
</p>
<p>
<label for="pump">Pump: </label>
<select name="pump" id="pump">
<option value="0">Not pumping</option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
</select>
<input name="lucky" type="checkbox" id="lucky"> <label for="lucky">Use a Lucky Shot</label>
</p>
</div>
<br>

<div class="box">
<p class="boxtitle">Target</p>
<p>
<?php
echo "<input name='target' class='small' required pattern='[0-9]+'";
if (isset($_POST["target"]))
	echo " value='".intval($_POST["target"])."'";
echo ">&nbsp;";
?>
<input type="submit" name="btnSubmit" value="Roll the Dice" id="btnSubmit">
</p>
</div>

</form>

<?php
if ($log != "") {
	echo "<div class='box' id='results'><h2>Results</h2>\n";
	echo "<p>$log</p></div>\n";
}

require("inc_foot.php");
?>
