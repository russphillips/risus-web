<?php
/*
Risus Web. Copyright (c) 2016 Robin Phillips
This software may be modified and distributed under the terms
of the MIT license.  See the LICENSE file for details.

Risus: The Anything RPG is written by S. John Ross. Get it from
https://www.drivethrurpg.com/product/170294/
*/

require("inc_head_php.php");
$title = "About";
require("inc_head_html.php");
?>

<h1>About Risus Web</h1>

<p>
Risus Web is written by <a href="https://rpg.phillipsuk.org/">Robin Phillips</a>. It is intended to be used by people playing online games of <a href="https://www.drivethrurpg.com/product/170294/?affiliate_id=235519">Risus: The Anything RPG</a> by S. John Ross. It can send updates (dice roll results etc) to an <a href="https://xmpp.org/">XMPP/Jabber</a> chatroom. It is open source, released under the MIT licence.
</p>
<p>
The source code is hosted on <a href="https://gitlab.com/robinphillips/risus-web" target="_blank">GitLab</a>. Please use the GitLab project to report bugs, request features, etc.
</p>

<p>The MIT License (MIT)</p>

<p>Risus Web. Copyright &copy; 2016 Robin Phillips</p>

<p>
Permission is hereby granted, free of charge, to any person obtaining a copy<br/>
of this software and associated documentation files (the &ldquo;Software&rdquo;), to deal<br/>
in the Software without restriction, including without limitation the rights<br/>
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell<br/>
copies of the Software, and to permit persons to whom the Software is<br/>
furnished to do so, subject to the following conditions:
</p>

<p>
The above copyright notice and this permission notice shall be included in all<br/>
copies or substantial portions of the Software.
</p>

<p>
THE SOFTWARE IS PROVIDED &ldquo;AS IS&rdquo;, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR<br/>
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,<br/>
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE<br/>
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER<br/>
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,<br/>
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE<br/>
SOFTWARE.
</p>

<hr />

<p>
Risus: The Anything RPG is written by S. John Ross. Get it from<br/>
https://www.drivethrurpg.com/product/170294/
</p>

<?php
require("inc_foot.php");
?>
