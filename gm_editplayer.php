<?php
/*
Risus Web. Copyright (c) 2016 Robin Phillips
This software may be modified and distributed under the terms
of the MIT license.  See the LICENSE file for details.

Risus: The Anything RPG is written by S. John Ross. Get it from
https://www.drivethrurpg.com/product/170294/
*/

require("inc_head_php.php");
require("inc_head_html.php");
$updated = "";

if (isset($_POST["btnSubmit"])) {
	// Get value of GM checkbox
	if (isset($_POST["gm"]) && $_POST["gm"] == 1)
		$gm = 1;
	else
		$gm = 0;
	// Get password hash
	if (isset($_POST["password"]) && $_POST["password"] != "")
		$pwhash = $db->escapeString(password_hash ($_POST["password"], PASSWORD_DEFAULT));
	else
		$pwhash = False;
}

if (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] == "Add Player") {
	// Set up and run INSERT query
	$sql = "INSERT INTO players (name, player_charid, password, gm, email) VALUES (
		'".$db->escapeString($_POST["name"])."',
		".$_POST["character"].",
		'$pwhash',
		$gm,
		'".$db->escapeString($_POST["email"])."')";
	$db->exec($sql);
	$updated = "Player details updated.";
}
elseif (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] == "Update Player") {
	// Set up SQL to update password if required
	if ($pwhash !== False)
		$passwordsql = "password = '$pwhash',";
	else
		$passwordsql = "";
	// Set up and run UPDATE query
	$sql = "UPDATE players SET
		name = '".$db->escapeString($_POST["name"])."',
		email = '".$db->escapeString($_POST["email"])."',
		$passwordsql
		player_charid = ".$_POST["character"].",
		gm = $gm
		WHERE playerid = ".intval($_POST["playerid"]);
	$db->exec($sql);
	
	$updated = "Player details updated.";
}
?>

<script>
$(function(){
	// Show add controls
	$("#addnewplayer").click(function() {
		// Clear form
		$("#gm").prop("checked", false)
		$("#password").val("")
		$("#password2").val("")
		// Set submit button text
		$("#btnSubmit").val("Add Player")
		// Title
		$(".boxtitle").text("Add New Player")
		$("#editform").show()
	})
	
	// Show edit controls
	$(".editbutton").click(function() {
		// Get player's ID
		pid=$(this).data("pid")
		$("#playerid").val(pid)
		// Populate form
		$("#name").val($("#name"+pid).text())
		$("#email").val($("#email"+pid).text())
		$("#character").val($("#char"+pid).data("charid"))
		if($("#gm"+pid).data("gm") == "1")
			$("#gm").prop("checked", true)
		else
			$("#gm").prop("checked", false)
		// Ensure password boxes are blank
		$("#password").val("")
		$("#password2").val("")
		// Set submit button text
		$("#btnSubmit").val("Update Player")
		// Title
		$(".boxtitle").text("Edit "+$("#name"+pid).text())
		// Show form
		$("#editform").show()
	})

	// Validate form
	$("#editform").submit(function (evt) {
		msg = ""
		
		if ($("#password").val().length > 0 && $("#password").val().length < 8) {
			if (msg != "")
				msg += "<br>"
			msg = "The new password must be at least eight characters long"
		}
		if ($("#password").val() != $("#password2").val()) {
			if (msg != "")
				msg += "<br>"
			msg += "The passwords do not match"
		}
		
		if (msg != "") {
			$("#msg").html(msg).show()
			evt.preventDefault()
		}
	})
})
</script>

<h1>Edit Player</h1>

<?php
if ($updated != "")
	echo "<p class='good'>$updated</p>";
?>
<p id="msg" class="bad hidden;"></p>

<table>
<tr>
<th>Name</th>
<th>Email</th>
<th>GM</th>
<th>Character</th>
<th>&nbsp;</th>
</tr>
<?php
$sql = "SELECT playerid, players.name AS pname, email, gm, player_charid, characters.name AS cname
	FROM players
	LEFT JOIN characters
	ON player_charid = charid
	ORDER BY pname";
$players = $db->query($sql);
while ($player = $players->fetchArray (SQLITE3_ASSOC)) {
	$pid = $player["playerid"];
	echo "<tr class='alternate'>
	<td id='name$pid'>".htmlentities($player["pname"], ENT_QUOTES)."</td>
	<td id='email$pid'>".htmlentities($player["email"], ENT_QUOTES)."</td>
	<td class='check' id='gm$pid' data-gm='".$player["gm"]."'>";
	if ($player["gm"] == 1)
		echo "✔";
	echo "</td>
	<td id='char$pid' class='check' data-charid='".$player["player_charid"]."'>";
	echo htmlentities($player["cname"], ENT_QUOTES)."</td>
	<td><button class='editbutton' data-pid='".$player["playerid"]."'>Edit</button></td>
	</tr>\n";
}
?>
</table>

<p>
<button id="addnewplayer">Add a new player</button>
</p>

<form method="post" id="editform" style="display:none;">
<input type="hidden" name="playerid" id="playerid">
<div class="box">
<p class="boxtitle"></p>
<p>
Name: <input name="name" required id="name"><br>
Email: <input name="email" required id="email" type="email"><br>
Character:
<select name="character" id="character">
<option value="0">No character</option>
<?php
selectCharacters($db, 0, 0);
?>
</select><br>
<input type="checkbox" name="gm" id="gm" value="1"> <label for="gm">This player is a GM</label>
</p>
<p>
Reset password: <input type="password" name="password" id="password"><br>
Confirm password: <input type="password" name="password2" id="password2">
</p>
<p>
<input type="submit" value="Update Player" name="btnSubmit" id="btnSubmit">
</p>
</div>
</form>

<?php
require("inc_foot.php");
?>
