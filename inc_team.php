<?php
/*
Risus Web. Copyright (c) 2016 Robin Phillips
This software may be modified and distributed under the terms
of the MIT license.  See the LICENSE file for details.

Risus: The Anything RPG is written by S. John Ross. Get it from
https://www.drivethrurpg.com/product/170294/
*/

$viewlog = "";

function displayCharacters($db, $npc, $show=True) {
	echo "<table id='table$npc'";
	if ($show === False)
		echo " style='display:none;'";
	echo "><tr><th>Member <input type='checkbox' id='allmember$npc' title='check/uncheck all'></th><th>Leader</th><th>Name</th><th>Cliché</th><th>Lucky Shot <input type='checkbox' id='alllucky$npc' title='check/uncheck all'></th><th>Pump</th><th>Drop Out</th></tr>";
	
	$sql = "SELECT * FROM characters WHERE active = 1 AND npc = $npc ORDER BY name";
	$pcs = $db->query($sql);
	while ($pc = $pcs->fetchArray(SQLITE3_ASSOC)) {
		echo "<tr class='alternate'>";
		if (isset($_POST["member".$pc["charid"]]) && $_POST["member".$pc["charid"]] != "")
			$checked = " checked";
		else
			$checked = "";
		echo "<td class='check'><input class='member$npc' type='checkbox' value='".$pc["charid"]."' name='member".$pc["charid"]."'$checked></td>";
		// Radio group on initially displayed table should be required
		if ($show !== False)
			$required = "required";
		else
			$required = "";
		if (isset ($_POST["leader$npc"]) && $_POST["leader$npc"] == $pc["charid"])
			$checked = " checked";
		else
			$checked = "";
		echo "<td class='check'><input type='radio' $required name='leader$npc' value='".$pc["charid"]."' class='leader$npc'$checked></td>";
		echo "<td>".htmlentities($pc["name"])."";
		echo "<input type='hidden' value='".htmlentities($pc["name"],ENT_QUOTES)."' name='name".$pc["charid"]."'></td>";
		
		echo "<td><select id='cliche".$pc["charid"]."' name='cliche".$pc["charid"]."'>";
		$sql = "SELECT * FROM cliches WHERE cliche_charid = ".$pc["charid"]." ORDER BY full DESC";
		$cliches = $db->query($sql);
		while ($cliche = $cliches->fetchArray(SQLITE3_ASSOC)) {
			if (isset($_POST["cliche".$pc["charid"]]) && $_POST["cliche".$pc["charid"]] == $cliche["clicheid"])
				$selected = " selected";
			else
				$selected = "";
			echo "<option id='teamoptioncliche".$cliche["clicheid"]."' value='".$cliche["clicheid"]."'$selected>".htmlentities($cliche["cliche"], ENT_QUOTES) . " ".$cliche["current"]." ".clichevalue($cliche["full"],$cliche["doublepump"])."</option>";
		}
		echo "</select></td>";

		echo "<td class='check'><input class='lucky$npc' type='checkbox' name='lucky".$pc["charid"]."'></td>";
		echo "<td class='check'><select name='pump".$pc["charid"]."'><option value='0'>Not pumping</option>";
		for ($i=1; $i<=12; $i++)
			echo "<option value='$i'>$i</option>";
		echo "</select></td>";
		echo "<td><button class='dropoutbutton' id='button".$pc["charid"]."' data-charid='".$pc["charid"]."' data-charname='".htmlentities($pc["name"],ENT_QUOTES)."'>Drop Out</button></td>";
		echo "</tr>";
	}
	echo "<tr class='inappropriate'><td colspan='7'>";
	echo "<input type='checkbox' name='inappropriate$npc' id='inappropriate$npc' value='1' title='Tick this box if the entire team&apos;s clichés are equally inappropriate'><label for='inappropriate$npc' id='lblinappropriate$npc'> Tick if all clichés are inappropriate</label><br>";
	echo "<input type='checkbox' name='vengeance$npc' id='vengeance$npc' value='1' title='If the team lost last round, and a member stepped forward voluntarily and took double damage, tick this box for the team leader to get double dice as a &quot;vengeance&quot; bonus'><label for='vengeance$npc' id='lblvengeance$npc'> Team leader gets double dice (&quot;vengeance&quot; bonus)</label>";
	echo "<button style='float:right;' id='breakup$npc' title='Click this button if the team breaks up voluntarily'>Break the team up</button>";
	echo "</td></tr>";
	echo "</table>";
}

/*
Function to roll a team's dice. Returns total
*/
function roll_team_dice ($db, $teamdice, $leaderid, $vengeance, &$log) {
	$teamtotal = 0;
	
	foreach ($teamdice as $member=>$dice) {
		$clichedice = $dice;
		
		if ($member == $leaderid) {
			$role = "<strong>Team leader</strong>";
			if ($vengeance == 1) {
				$role .= " (with vengeance bonus)";
				$dice = $dice * 2;
			}
		}
		else
			$role = "Team member";
		// Roll the dice
		$add = 0;
		// Lucky Shot
		if (isset($_POST["lucky".$member])) {
			$lucky = " Using a lucky shot.";
			$dice++;
		}
		else
			$lucky = "";

		// Pump
		$pump = intval($_POST["pump".$member]);
		if ($pump > 0) {
			$pumplog = " Pumping by $pump ".die_dice($pump).".";
			$dice += $pump;
			$new = $clichedice - $pump;
			if ($new < 0)
				$new = 0;
			$sql = "UPDATE cliches SET current = $new WHERE clicheid = ".$_POST["cliche$member"];
			// echo $sql;
			$db->exec($sql);
		}
		else
			$pumplog = "";

		$d = dice_roll($dice);
		// Work out result
		$log .= "$role ".htmlentities($_POST["name".$member], ENT_QUOTES).".";
		$log .= $lucky . $pumplog . " Rolling $dice ".die_dice($dice).": (";

		foreach ($d as $die) {
			$log .= "$die, ";
			if (strpos($role, "leader") !== False || $die == 6)
				$add += $die;
		}
		// Remove final comma-space
		$log = substr($log, 0, -2) . ")<br>";
		$teamtotal += $add;
	}
	return $teamtotal;
}
?>

<script>
$(function() {
	// Copy titles from checkboxes to labels. Done this way to ensure they are the same
	$("#lblinappropriate0").prop("title", $("#inappropriate0").prop("title"))
	$("#lblinappropriate1").prop("title", $("#inappropriate1").prop("title"))
	$("#lblvengeance0").prop("title", $("#vengeance0").prop("title"))
	$("#lblvengeance1").prop("title", $("#vengeance1").prop("title"))
	
	// Change handlers for select/deselect all
	$("#allmember0").change(function() {
		$(".member0").prop('checked', $(this).prop("checked"))
	})
	$("#allmember1").change(function() {
		$(".member1").prop('checked', $(this).prop("checked"))
	})
	$("#alllucky0").change(function() {
		$(".lucky0").prop('checked', $(this).prop("checked"))
	})
	$("#alllucky1").change(function() {
		$(".lucky1").prop('checked', $(this).prop("checked"))
	})

	// Show/hide table
	$("#changeteam").click(function() {
		// Hide/show tables
		$("#table0").toggle()
		$("#table1").toggle()
		if ($("#teamtype").val() == "character") {
			// Switch to NPC team
			$("#currentteam").text ("NPC")
			$("#teamtype").val("npc")
			$("#changeteam").text ("Change to Character team")
			// Toggle 'required' property of leader radiogroups
			$(".leader0").prop('required', false)
			$(".leader1").prop('required', true)
			// Unselect all characters so that they are not included in dice rolls
			$("#allmember0").prop('checked', false)
			$(".member0").prop('checked', false)
			$(".leader0").prop('checked', false)
		}
		else {
			// Switch to character team
			$("#currentteam").text ("Character")
			$("#teamtype").val("character")
			$("#changeteam").text ("Change to NPC team")
			// Toggle 'required' property of leader radiogroups
			$(".leader0").prop('required', true)
			$(".leader1").prop('required', false)
			// Unselect all NPCs so that they are not included in dice rolls
			$("#allmember1").prop('checked', false)
			$(".member1").prop('checked', false)
			$(".leader1").prop('checked', false)
		}
	})
	
	// Use AJAX to update team member that has been chosen to take damage
	$("#selectbutton").click(function(event) {
		clicheid = $("#updateteamcliche").val()
		// Double damage if stepping forward
		if ($("#stepforward").prop("checked"))
			newvalue = $("#option"+clicheid).data("current") - ($("#updateteamcliche").data("clicheloss") * 2)
		else
			newvalue = $("#option"+clicheid).data("current") - $("#updateteamcliche").data("clicheloss")
		// Value cannot go below zero
		if (newvalue < 0)
			newvalue = 0
		// Apply the damage
		updateCliche(newvalue, clicheid, $("#updateteamcliche option:selected").text())
		
		leader0id = $("input[name='leader0']:checked").val()
		leader0clicheid = $("#cliche"+leader0id+" option:selected").val()
		leader1id = $("input[name='leader1']:checked").val()
		leader1clicheid = $("#cliche"+leader1id+" option:selected").val()

		// Check if leader is defeated
		if ((leader0clicheid == clicheid || leader1clicheid == clicheid) && newvalue == 0) {
			displayLog("<br>Team leader is defeated. Team disbands, everyone suffering 1 damage")
			
			if (leader0clicheid == clicheid)
				members = $("input[class='member0']:checked")
			else
				members = $("input[class='member1']:checked")
			
			members.each(function(index) {
				charid=$(this).val()
				clicheid=$("#cliche"+charid+" option:selected").val()
				charname=$("input[name='name"+charid+"']").val()
				// Ignore defeated team leader - their cliché is already at zero
				if (clicheid != leader0clicheid && clicheid != leader1clicheid)
					updateCliche(-1, clicheid, charname)
			})
		}
	})
	
	// Use AJAX to update team member that drops out
	$(".dropoutbutton").click(function(event) {
		charid = $(this).data("charid")
		charname = $(this).data("charname")
		clicheid = $("#cliche"+charid).val()
				
		$.ajax ({
			// Apply the damage
			url: "./ajax_updatecliche.php?newvalue=0&clicheid="+clicheid
		})
		 .done (function() {
			// Update cliché
			$('#cliche'+charid).load ('./ajax_clicheoptionlist.php?charid='+charid, function() {
				// Make sure used cliché is selected
				$('#cliche'+charid).val(clicheid)
			})
		})
	})

	function updateCliche(newvalue, clicheid, charname) {
		$.ajax ({
			// Apply the damage
			url: "./ajax_updatecliche.php?newvalue="+newvalue+"&clicheid="+clicheid
		})
		.done (function() {
			// Hide the "stepping forward" controls since they are no longer needed
			$("#updateteamcliche").hide()
			$("#selectbutton").hide()
			$("#stepforward").hide()
			$("#lblStepForward").hide()
			
			logstr = "<br>" + charname + " takes damage"
			// Show damage details
			if ($("#stepforward").prop("checked") == true)
				logstr += " (stepped forward)"

			// Show notice if team member is defeated
			if (newvalue == 0)
				logstr += "<br><strong>"+$("#updateteamcliche option:selected").text()+" is defeated.</strong>"

			// Update cliché list for team member that took damage
			charid = $("#option"+clicheid).data("charid")
			$('#cliche'+charid).load ('./ajax_clicheoptionlist.php?charid='+charid, function() {
				// Make sure used cliché is selected
				$('#cliche'+charid).val(clicheid)
			})
			
			// Display extra log
			displayLog(logstr)
		})
	}

	function displayLog(logstr) {
		// Display extra log
		$("#viewlog").html($("#viewlog").html() + logstr)
		// Save extra log to database and send to XMPP chatroom
		$.post("./ajax_log.php", {
			value: logstr
		})
	}

	// Breakup buttons
	$("#breakup0").click(function() {
		displayLog("Character team breaking up<br>")
		$("input[class='member0']:checked").each(function(index) {
			charid=$(this).val()
			clicheid=$("#cliche"+charid+" option:selected").val()
			charname=$("input[name='name"+charid+"']").val()
			updateCliche(-1, clicheid, charname)
		})
	})
	$("#breakup1").click(function() {
		displayLog("NPC team breaking up<br>")
		$("input[class='member1']:checked").each(function(index) {
			charid=$(this).val()
			clicheid=$("#cliche"+charid+" option:selected").val()
			charname=$("input[name='name"+charid+"']").val()
			updateCliche(-1, clicheid, charname)
		})
	})
})
</script>
