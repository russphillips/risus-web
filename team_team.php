<?php
/*
Risus Web. Copyright (c) 2016 Robin Phillips
This software may be modified and distributed under the terms
of the MIT license.  See the LICENSE file for details.

Risus: The Anything RPG is written by S. John Ross. Get it from
https://www.drivethrurpg.com/product/170294/
*/

require("inc_head_php.php");
require("inc_head_html.php");
require("inc_team.php");

// Get team members
if (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] != "") {
	$log = "<p>Team vs Team</p>\n<p>";

	// PCs
	$team0cliches = array();
	$team0dice = array();
	$team0total = 0;
	// NPCs
	$team1cliches = array();
	$team1dice = array();
	$team1total = 0;
	
	// Get "inappropriate" and "vengeance" states and ensure that "member" checkbox is set to checked for leader
	if (isset($_POST["inappropriate0"]))
		$team0inappropriate = intval($_POST["inappropriate0"]);
	else
		$team0inappropriate = 0;
	if (isset($_POST["vengeance0"]))
		$team0vengeance = intval($_POST["vengeance0"]);
	else
		$team0vengeance = 0;
	$_POST["member".intval($_POST["leader0"])] = intval($_POST["leader0"]);
	
	// Get "inappropriate" and "vengeance" states and ensure that "member" checkbox is set to checked for leader
	if (isset($_POST["inappropriate1"]))
		$team1inappropriate = intval($_POST["inappropriate1"]);
	else
		$team1inappropriate = 0;
	if (isset($_POST["vengeance1"]))
		$team1vengeance = intval($_POST["vengeance1"]);
	else
		$team1vengeance = 0;
	$_POST["member".intval($_POST["leader1"])] = intval($_POST["leader1"]);
	
	// Get all clichés
	$sql = "SELECT clicheid, charid, current, npc FROM cliches LEFT JOIN characters on cliche_charid=charid";
	$result = $db->query($sql);
	// Find the ones that were selected and get their current values
	while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
		if (isset($_POST["member".$row["charid"]]) && $_POST["cliche".$row["charid"]] == $row["clicheid"] && $row["npc"] == 0) {
			$team0cliches [$row["charid"]] = $row["clicheid"];
			$team0dice [$row["charid"]] = $row["current"];
		}
		elseif (isset($_POST["member".$row["charid"]]) && $_POST["cliche".$row["charid"]] == $row["clicheid"] && $row["npc"] == 1) {
			$team1cliches [$row["charid"]] = $row["clicheid"];
			$team1dice [$row["charid"]] = $row["current"];
		}
	}

	// Roll dice
	$team0total = roll_team_dice ($db, $team0dice, $_POST["leader0"], $team0vengeance, $log);
	$log .= "Character team total is <strong>$team0total</strong>.</p>\n";
	$team1total = roll_team_dice ($db, $team1dice, $_POST["leader1"], $team1vengeance, $log);
	$log .= "NPC team total is <strong>$team1total</strong>.</p>\n";

	// Work out results
	if ($team0total == $team1total) {
		$log .= "Draw!";
		$teamcliches = array ();
		$teamclicheloss = 0;
	}
	elseif ($team0total > $team1total) {
		// Character team won
		$log .= "Character team wins.";
		$teamcliches = $team1cliches;
		
		// $viewlog is HTML to be displayed but not logged
		$viewlog = "Choose a team member to lose ";
		if ($team0inappropriate == 1 && $team1inappropriate == 0) {
			$viewlog .= "<i>three points</i>";
			$teamclicheloss = 3;
		}
		else {
			$viewlog .= "one point:";
			$teamclicheloss = 1;
		}
	}
	else {
		// NPC team won
		$log .= "NPC team wins.";
		$teamcliches = $team0cliches;

		// $viewlog is HTML to be displayed but not logged
		$viewlog = "Choose a team member to lose ";
		if ($team1inappropriate == 1 && $team0inappropriate == 0) {
			$viewlog .= "<i>three points</i>";
			$teamclicheloss = 3;
		}
		else {
			$viewlog .= "one point:";
			$teamclicheloss = 1;
		}
	}
	
	if ($team0total != $team1total) {
		// Not a draw. Show SELECT etc for choosing team member that will take damage
		$viewlog .= "<br><select id='updateteamcliche' data-clicheloss='$teamclicheloss'>";
		foreach ($teamcliches as $charid=>$clicheid) {
			$name = $db->querySingle("SELECT name FROM characters WHERE charid = $charid");
			$cliche = $db->querySingle("SELECT cliche, current FROM cliches WHERE clicheid = $clicheid", True);
			$viewlog .= "<option value='$clicheid' id='option$clicheid' data-charid='$charid' data-current='".intval($cliche["current"])."'>";
			$viewlog .= htmlentities($name, ENT_QUOTES)." (".htmlentities($cliche["cliche"], ENT_QUOTES)." ".intval($cliche["current"]).")";
			$viewlog .= "</option>";
		}
		$viewlog .= "</select>";
		$viewlog .= "&nbsp;<button id='selectbutton'>Select</button><br>";
		$viewlog .= "<input type = 'checkbox' name='stepforward' id='stepforward' value = '1'> <label for ='stepforward' title='The character takes double damage, but the team leader gets double dice on the next roll' id='lblStepForward'>Character is stepping forward</label>";
	}
	
	$log .= "</p>\n";
	$viewlog .= "</p>\n";
	logdb ($log);
}
?>

<script>
$(function() {
	// Hide results box on rolling dice
	$("#btnSubmit").click(function(event) {
		$("#resultsdiv").hide()
	})
})
</script>

<h1>Combat: Team vs Team</h1>

<form method="post">

<p>Character Team</p>

<?php
displayCharacters($db, 0);
?>
<p>vs NPC Team</p>
<?php
displayCharacters($db, 1);
?>

<p><input type="submit" name="btnSubmit" value="Roll the Dice" id="btnSubmit"></p>
</form>

<?php
if ($log != "")
	$style = "";
else
	$style = "style='display:none;'";
?>
<div class='box' id='resultsdiv' <?=$style;?>>
<h2>Results</h2>
<?php
echo "<p>$log</p>\n";
if ($viewlog != "")
	$style = "";
else
	$style = "style='display:none;'";
echo "<p id='viewlog' $style>$viewlog</p>\n";
?>
</div>

<?php
require("inc_foot.php");
?>
